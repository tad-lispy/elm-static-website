module Routes exposing
    ( Route(..)
    , parse
    , parser
    )

import Url exposing (Url)
import Url.Parser as Parser exposing (..)


type Route
    = Home
    | Content String
    | NotFound


parser : Parser (Route -> b) b
parser =
    Parser.oneOf
        [ Parser.map Home Parser.top
        , Parser.custom "Content"
            (\path ->
                case String.split "." path of
                    [ base, "html" ] ->
                        Just (Content base)

                    _ ->
                        Nothing
            )
        ]


parse : Url -> Route
parse url =
    url
        |> Parser.parse parser
        |> Maybe.withDefault NotFound
