module Protractor exposing (protractor)

import Axis2d exposing (Axis2d)
import Circle2d
import Element exposing (fill)
import Geometry.Svg
import LineSegment2d exposing (LineSegment2d)
import Point2d
import Svg exposing (Svg)
import Svg.Attributes exposing (fill, stroke, viewBox)


type alias Config =
    { radius : Float
    , strokeWidth : Float
    }


protractor : Config -> Svg msg
protractor config =
    let
        mark : Float -> Svg msg
        mark direction =
            if remainderBy 10 (floor direction) == 0 then
                LineSegment2d.along (axis direction) (config.radius * 0.94) config.radius
                    |> Geometry.Svg.lineSegment2d strokeAttributes

            else if remainderBy 5 (floor direction) == 0 then
                LineSegment2d.along (axis direction) (config.radius * 0.96) config.radius
                    |> Geometry.Svg.lineSegment2d strokeAttributes

            else
                LineSegment2d.along (axis direction) (config.radius * 0.98) config.radius
                    |> Geometry.Svg.lineSegment2d strokeAttributes

        axis : Float -> Axis2d
        axis direction =
            Axis2d.x
                |> Axis2d.rotateAround Point2d.origin (degrees direction)

        marks : List (Svg msg)
        marks =
            List.range 0 359
                |> List.map toFloat
                |> List.map mark

        edge : Svg msg
        edge =
            Circle2d.withRadius config.radius Point2d.origin
                |> Geometry.Svg.circle2d
                    [ Svg.Attributes.stroke "black"
                    , Svg.Attributes.fill "none"
                    ]

        labels : List (Svg msg)
        labels =
            List.range 0 35
                |> List.map ((*) 10)
                |> List.map String.fromInt
                |> List.map label

        label : String -> Svg msg
        label direction =
            Svg.text_
                [ Svg.Attributes.x "0"
                , Svg.Attributes.y "0"
                , Svg.Attributes.fill "black"
                , Svg.Attributes.transform ("rotate (" ++ direction ++ ") translate (" ++ String.fromFloat (config.radius * 0.89) ++ ")")
                , Svg.Attributes.fontSize <| String.fromFloat <| config.radius * 0.05
                , Svg.Attributes.dominantBaseline "central"
                , Svg.Attributes.textAnchor "middle"
                ]
                [ Svg.text direction ]

        axes : List (Svg msg)
        axes =
            [ LineSegment2d.along Axis2d.x (config.radius * 0.84 * -1) (config.radius * 0.84)
            , LineSegment2d.along Axis2d.y (config.radius * 0.84 * -1) (config.radius * 0.84)
            ]
                |> List.map (Geometry.Svg.lineSegment2d strokeAttributes)

        strokeAttributes =
            [ stroke "gray"
            , Svg.Attributes.strokeWidth (String.fromFloat config.strokeWidth)
            ]
    in
    edge
        :: marks
        |> List.append labels
        |> List.append axes
        |> Svg.g
            []
