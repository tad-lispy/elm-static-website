puppeteer = require "puppeteer"
express = require "express"
FS = require "fs"
glob = require "glob"
Path = require "path"

match = (pattern, options) =>
  new Promise (resolve, reject) =>
    glob pattern, options, (error, paths) =>
      if error then return reject errro
      resolve paths

do () =>
  app = express ``
  app.use express.static "public/"

  app.get '*', (req, res) ->
    res.sendFile "container.html", root: process.cwd ``

  server = app.listen 8000

  browser = await puppeteer.launch
    args: [
      "--no-sandbox"
      "--disable-setuid-sandbox"
    ]

  page = await browser.newPage ``

  for match in await match "*.txt", cwd: "content/"
    await do (match) ->
      { dir, name } = Path.parse match
      base = if dir is "" then name else "#{dir}/#{name}"

      url = "http://localhost:8000/#{base}.html"
      path = "built/captured/#{base}.html"

      await page.goto url, waitUntil: "networkidle2"

      html =
        (await page.content ``)
          .replace /<p\b/gi, "<div"
          .replace /<\/p\b/gi, "</div"
          .replace "</body>", """
              <script src="/built/index.js"></script>
              <script>
                Elm.Main.init()
              </script>
            </body>
          """

      await new Promise (resolve, reject) ->
        FS.writeFile path, html, (error) ->
          if error then return reject error
          do resolve

  await browser.close ``
  server.close ``
