module Examples.Editor exposing (main)

import Editor exposing (editor)
import Element exposing (Element)
import Html exposing (Html)
import Set exposing (Set)


main : Html msg
main =
    """This is an example of how Editor module can be used.

It supports highligs. Lines can be folded, like this

This code is folded

So is this.

Highlights can span over multiple lines. Try:

"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
culpa qui officia deserunt mollit anim id est laborum."
    |> editor Editor.defaults
        { highlights =
            [ { top = 10, left = 1, width = 40, height = 7 }
            , { top = 3, left = 1, width = 15, height = 1 }
            , { top = 19, left = 1, width = 10, height = 1 }
            , { top = 20, left = 15, width = 12, height = 1 }
            ]
        , folded =
            [ List.range 4 6
            , List.range 74 91
            ]
                |> List.concat
                |> Set.fromList
        }
    |> Element.layout
        [ Element.width Element.fill
        , Element.height Element.fill
        ]


"""
        |> editor Editor.defaults
            { highlights =
                [ { top = 3, left = 13, width = 8, height = 1 }
                , { top = 19, left = 15, width = 45, height = 4 }
                ]
            , folded =
                [ List.range 5 7
                , List.range 12 14
                ]
                    |> List.concat
                    |> Set.fromList
            }
        |> Element.layout
            [ Element.width Element.fill
            , Element.height Element.fill
            ]
