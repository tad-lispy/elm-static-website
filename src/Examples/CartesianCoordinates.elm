module Examples.CartesianCoordinates exposing
    ( Config
    , Model
    , Msg
    , init
    , main
    , ui
    , update
    , view
    )

import Browser
import CartesianPlane exposing (graph)
import Element exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Element.Input as Input
import Html exposing (Html)
import Svg exposing (..)
import Svg.Attributes exposing (..)


type alias Config =
    { minX : Float
    , minY : Float
    , maxX : Float
    , maxY : Float
    }


defaults : Config
defaults =
    { minX = -150
    , minY = -150
    , maxX = 150
    , maxY = 150
    }


main : Program () Model Msg
main =
    Browser.sandbox
        { init = init
        , view = view
        , update = update
        }


type alias Model =
    { x : Float
    , y : Float
    }


type Msg
    = SetX Float
    | SetY Float


init : Model
init =
    { x = 0, y = 0 }


view : Model -> Html Msg
view model =
    model
        |> ui defaults
        |> Element.el
            [ Element.width (Element.maximum 600 Element.fill)
            , Element.centerX
            ]
        |> Element.layout
            [ Element.height Element.fill
            , Element.width Element.fill
            ]


update : Msg -> Model -> Model
update msg model =
    case msg of
        SetX x ->
            { model | x = x }

        SetY y ->
            { model | y = y }


ui : Config -> Model -> Element Msg
ui config model =
    let
        graph =
            [ circle
                [ cx <| String.fromFloat model.x
                , cy <| String.fromFloat model.y
                , r "2"
                , fill "magenta"
                ]
                []
            , text_
                [ x <| String.fromFloat (model.x + 5)
                , y <| String.fromFloat (model.y + 5)
                , fontSize "6"
                , fontFamily "Source Code Pro, monospace"
                , fill "gray"
                , dominantBaseline "central"
                ]
                [ text coordinates ]
            ]
                |> CartesianPlane.graph config
                |> Element.html
                |> Element.el
                    [ Element.height Element.fill
                    , Element.width Element.fill
                    ]

        coordinates =
            "{ x = "
                ++ String.fromFloat model.x
                ++ ", y = "
                ++ String.fromFloat model.y
                ++ "}"
    in
    Element.column
        [ Element.width Element.fill
        , Element.centerX
        , Element.spacing 30
        , Element.padding 30
        ]
        [ graph
        , Input.slider
            [ Element.behindContent
                (Element.el
                    [ Element.width Element.fill
                    , Element.height (Element.px 2)
                    , Element.centerY
                    , Background.color <| Element.rgb 0.7 0.7 0.7
                    , Border.rounded 2
                    ]
                    Element.none
                )
            ]
            { onChange = SetX
            , label =
                Input.labelBelow [ Element.centerX ] <|
                    Element.text ("x = " ++ String.fromFloat model.x)
            , min = config.minX
            , max = config.maxX
            , value = model.x
            , thumb = Input.defaultThumb
            , step = Just 0.01
            }
        , Input.slider
            [ Element.behindContent
                (Element.el
                    [ Element.width Element.fill
                    , Element.height (Element.px 2)
                    , Element.centerY
                    , Background.color <| Element.rgb 0.7 0.7 0.7
                    , Border.rounded 2
                    ]
                    Element.none
                )
            ]
            { onChange = SetY
            , label =
                Input.labelBelow [ Element.centerX ] <|
                    Element.text ("y = " ++ String.fromFloat model.y)
            , min = config.minY
            , max = config.maxY
            , value = model.y
            , thumb = Input.defaultThumb
            , step = Just 0.01
            }
        ]
