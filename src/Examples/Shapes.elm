module Examples.Shapes exposing
    ( Config
    , Container
    , Shape(..)
    , main
    , ui
    )

import Circle2d
import Element exposing (Element)
import Geometry.Svg
import Html exposing (Html)
import Html.Attributes
import LineSegment2d
import Point2d exposing (Point2d)
import Svg exposing (Attribute, Svg)
import Svg.Attributes
import Transformations


type alias Config =
    { container : Container
    , shapes : List Shape
    }


type alias Container =
    { background : String
    , viewBox : String
    , fill : Bool
    }


type Shape
    = Dot
        { cx : Float
        , cy : Float
        , radius : Float
        , color : String
        }
    | Line
        { x1 : Float
        , y1 : Float
        , x2 : Float
        , y2 : Float
        , color : String
        }
    | Group
        { rotation : Float
        , x : Float
        , y : Float
        }
        (List Shape)


defaults : Config
defaults =
    { container =
        { background = "none"
        , viewBox = "-300 -300 600 600"
        , fill = True
        }
    , shapes =
        [ Dot
            { cx = 0
            , cy = 0
            , radius = 10
            , color = "skyblue"
            }
        , Line
            { x1 = 0.0
            , y1 = 0.0
            , x2 = 20.0
            , y2 = 30.0
            , color = "green"
            }
        , Group { rotation = -45.0, x = 0.0, y = 0.0 }
            [ Line
                { x1 = 20.0
                , y1 = 0.0
                , x2 = 70.0
                , y2 = 0.0
                , color = "orange"
                }
            , Dot
                { cx = 100
                , cy = 0.0
                , radius = 20.0
                , color = "red"
                }
            , Group { rotation = -45.0, x = 100.0, y = 0.0 }
                [ Line
                    { x1 = 20.0
                    , y1 = 0.0
                    , x2 = 70.0
                    , y2 = 0.0
                    , color = "orange"
                    }
                , Dot
                    { cx = 100
                    , cy = 0.0
                    , radius = 20.0
                    , color = "red"
                    }
                ]
            ]
        ]
    }


main : Html msg
main =
    ui defaults
        |> Element.layout
            [ Element.width Element.fill
            , Element.height Element.fill
            ]


ui : Config -> Element msg
ui { container, shapes } =
    let
        shape : Shape -> Svg msg
        shape s =
            case s of
                Dot { cx, cy, color, radius } ->
                    ( cx, cy )
                        |> Point2d.fromCoordinates
                        |> Circle2d.withRadius radius
                        |> Geometry.Svg.circle2d [ Svg.Attributes.fill color ]

                Line { x1, y1, x2, y2, color } ->
                    let
                        a =
                            Point2d.fromCoordinates ( x1, y1 )

                        b =
                            Point2d.fromCoordinates ( x2, y2 )
                    in
                    LineSegment2d.from a b
                        |> Geometry.Svg.lineSegment2d
                            [ Svg.Attributes.stroke color
                            , Svg.Attributes.strokeWidth "1"
                            ]

                Group { rotation, x, y } ss ->
                    ss
                        |> List.map shape
                        |> Svg.g
                            [ [ Transformations.Rotate rotation
                              , Transformations.Translate x y
                              ]
                                |> Transformations.apply
                                |> Svg.Attributes.transform
                            ]

        width =
            if container.fill then
                "100%"

            else
                "300"

        height =
            if container.fill then
                "100%"

            else
                "150"

        padding =
            if container.fill then
                "0"

            else
                "5px"
    in
    shapes
        |> List.map shape
        |> Svg.svg
            [ Svg.Attributes.viewBox container.viewBox
            , Html.Attributes.style "background" container.background
            , Svg.Attributes.width width
            , Svg.Attributes.height height
            , Html.Attributes.style "margin" padding
            ]
        |> Element.html
