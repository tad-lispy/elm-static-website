module Examples.RosetteTypedTransformations exposing (main, ui)

import Element
import Svg
import Svg.Attributes


main =
    Element.layout
        [ Element.width Element.fill
        , Element.height Element.fill
        ]
        ui


ui =
    Element.html
        (Svg.svg
            [ Svg.Attributes.height "400"
            , Svg.Attributes.viewBox "-100 -100 200 200"
            ]
            [ Svg.defs []
                [ Svg.linearGradient
                    [ Svg.Attributes.id "blue-pink-gradient"
                    , Svg.Attributes.x1 "0"
                    , Svg.Attributes.y1 "0"
                    , Svg.Attributes.x2 "1"
                    , Svg.Attributes.y2 "0"
                    , Svg.Attributes.gradientUnits "userSpaceOnUse"
                    ]
                    [ Svg.stop
                        [ Svg.Attributes.stopColor "blue"
                        , Svg.Attributes.offset "0"
                        ]
                        []
                    , Svg.stop
                        [ Svg.Attributes.stopColor "pink"
                        , Svg.Attributes.offset "1"
                        ]
                        []
                    ]
                ]
            , Svg.line
                [ Svg.Attributes.x2 "1"
                , Svg.Attributes.stroke "url(#blue-pink-gradient)"
                , transform
                    [ Rotate 0
                    , Scale 100 1
                    ]
                ]
                []
            , Svg.line
                [ Svg.Attributes.x2 "1"
                , Svg.Attributes.stroke "url(#blue-pink-gradient)"
                , transform
                    [ Rotate 72
                    , Scale 100 1
                    ]
                ]
                []
            , Svg.line
                [ Svg.Attributes.x2 "1"
                , Svg.Attributes.stroke "url(#blue-pink-gradient)"
                , transform
                    [ Rotate 144
                    , Scale 100 1
                    ]
                ]
                []
            , Svg.line
                [ Svg.Attributes.x2 "1"
                , Svg.Attributes.stroke "url(#blue-pink-gradient)"
                , transform
                    [ Rotate 216
                    , Scale 100 1
                    ]
                ]
                []
            , Svg.line
                [ Svg.Attributes.x2 "1"
                , Svg.Attributes.stroke "url(#blue-pink-gradient)"
                , transform
                    [ Rotate 288
                    , Scale 100 1
                    ]
                ]
                []
            ]
        )


type Transformation
    = Identity
    | Scale Float Float
    | Translate Float Float
    | Rotate Float


transform : List Transformation -> Svg.Attribute msg
transform transformations =
    let
        toString : Transformation -> String
        toString transformation =
            case transformation of
                Identity ->
                    ""

                Scale x y ->
                    "scale("
                        ++ String.fromFloat x
                        ++ ", "
                        ++ String.fromFloat y
                        ++ ")"

                Translate x y ->
                    "translate("
                        ++ String.fromFloat x
                        ++ ", "
                        ++ String.fromFloat y
                        ++ ")"

                Rotate angle ->
                    "rotate("
                        ++ String.fromFloat angle
                        ++ ")"
    in
    transformations
        |> List.map toString
        |> String.join " "
        |> Svg.Attributes.transform
