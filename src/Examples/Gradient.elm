module Examples.Gradient exposing (main, ui)

import Element
import Svg
import Svg.Attributes


main =
    Element.layout
        [ Element.width Element.fill
        , Element.height Element.fill
        ]
        ui


ui =
    Element.html
        (Svg.svg
            [ Svg.Attributes.height "200"
            , Svg.Attributes.viewBox "-10 -25 100 100"
            ]
            [ Svg.defs []
                [ Svg.linearGradient
                    [ Svg.Attributes.id "blue-pink-gradient"
                    , Svg.Attributes.x1 "0"
                    , Svg.Attributes.y1 "0"
                    , Svg.Attributes.x2 "1"
                    , Svg.Attributes.y2 "0"
                    , Svg.Attributes.gradientUnits "userSpaceOnUse"
                    ]
                    [ Svg.stop
                        [ Svg.Attributes.stopColor "blue"
                        , Svg.Attributes.offset "0"
                        ]
                        []
                    , Svg.stop
                        [ Svg.Attributes.stopColor "pink"
                        , Svg.Attributes.offset "1"
                        ]
                        []
                    ]
                ]
            , Svg.line
                [ Svg.Attributes.x2 "1"
                , Svg.Attributes.stroke "url(#blue-pink-gradient)"
                , Svg.Attributes.transform "rotate(30) scale(100, 1)"
                ]
                []
            ]
        )
