module Rosette exposing (main)

import Element
import Svg
import Svg.Attributes


main =
    Element.layout
        [ Element.width Element.fill
        , Element.height Element.fill
        ]
        (Element.html
            (Svg.svg
                [ Svg.Attributes.viewBox "-100 -100 200 200" ]
                [ Svg.defs []
                    [ Svg.linearGradient
                        [ Svg.Attributes.id "blue-pink-gradient"
                        , Svg.Attributes.x1 "0"
                        , Svg.Attributes.y1 "0"
                        , Svg.Attributes.x2 "1"
                        , Svg.Attributes.y2 "0"
                        , Svg.Attributes.gradientUnits "userSpaceOnUse"
                        ]
                        [ Svg.stop
                            [ Svg.Attributes.stopColor "blue"
                            , Svg.Attributes.offset "0"
                            ]
                            []
                        , Svg.stop
                            [ Svg.Attributes.stopColor "pink"
                            , Svg.Attributes.offset "1"
                            ]
                            []
                        ]
                    ]
                , Svg.line
                    [ Svg.Attributes.x2 "1"
                    , Svg.Attributes.stroke "url(#blue-pink-gradient)"
                    , Svg.Attributes.transform "rotate(0) scale(100, 1)"
                    ]
                    []
                , Svg.line
                    [ Svg.Attributes.x2 "1"
                    , Svg.Attributes.stroke "url(#blue-pink-gradient)"
                    , Svg.Attributes.transform "rotate(72) scale(100, 1)"
                    ]
                    []
                , Svg.line
                    [ Svg.Attributes.x2 "1"
                    , Svg.Attributes.stroke "url(#blue-pink-gradient)"
                    , Svg.Attributes.transform "rotate(144) scale(100, 1)"
                    ]
                    []
                , Svg.line
                    [ Svg.Attributes.x2 "1"
                    , Svg.Attributes.stroke "url(#blue-pink-gradient)"
                    , Svg.Attributes.transform "rotate(216) scale(100, 1)"
                    ]
                    []
                , Svg.line
                    [ Svg.Attributes.x2 "1"
                    , Svg.Attributes.stroke "url(#blue-pink-gradient)"
                    , Svg.Attributes.transform "rotate(288) scale(100, 1)"
                    ]
                    []
                ]
            )
        )
