module Examples exposing (Model, Msg(..), init, subscriptions, update)

import Examples.CartesianCoordinates
import Examples.Circle
import Examples.Counter
import Examples.Gradient
import Examples.NestedTransformations
import Examples.PolarCoordinates
import Examples.RosetteTypedTransformations
import Examples.Shapes
import Examples.Spiral
import Examples.Transformations
import Examples.Tree
import Examples.ViewBox


type alias Model =
    { counter : Examples.Counter.Model
    , transformations : Examples.Transformations.Model
    , nestedTransformations : Examples.NestedTransformations.Model
    , cartesianCoordinates : Examples.CartesianCoordinates.Model
    , polarCoordinates : Examples.PolarCoordinates.Model
    , viewBox : Examples.ViewBox.Model
    }


type Msg
    = CounterMsg Examples.Counter.Msg
    | TransformationsMsg Examples.Transformations.Msg
    | NestedTransformationsMsg Examples.NestedTransformations.Msg
    | CartesianCoordinatesMsg Examples.CartesianCoordinates.Msg
    | PolarCoordinatesMsg Examples.PolarCoordinates.Msg
    | ViewBoxMsg Examples.ViewBox.Msg


init : ( Model, Cmd Msg )
init =
    ( { counter = Examples.Counter.init
      , transformations = Examples.Transformations.init
      , nestedTransformations = Examples.NestedTransformations.init
      , cartesianCoordinates = Examples.CartesianCoordinates.init
      , polarCoordinates = Examples.PolarCoordinates.init
      , viewBox = Examples.ViewBox.init
      }
    , Cmd.none
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        CounterMsg m ->
            ( { model | counter = Examples.Counter.update m model.counter }
            , Cmd.none
            )

        TransformationsMsg m ->
            ( { model
                | transformations =
                    Examples.Transformations.update m model.transformations
              }
            , Cmd.none
            )

        NestedTransformationsMsg m ->
            ( { model
                | nestedTransformations =
                    Examples.NestedTransformations.update m model.nestedTransformations
              }
            , Cmd.none
            )

        CartesianCoordinatesMsg m ->
            ( { model
                | cartesianCoordinates =
                    Examples.CartesianCoordinates.update m model.cartesianCoordinates
              }
            , Cmd.none
            )

        PolarCoordinatesMsg m ->
            ( { model
                | polarCoordinates =
                    Examples.PolarCoordinates.update m model.polarCoordinates
              }
            , Cmd.none
            )

        ViewBoxMsg m ->
            ( { model | viewBox = Examples.ViewBox.update m model.viewBox }, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none
